# FoxTail Android knowledge base

[Public URL](https://foxtaildev.gitlab.io/site/android-knowledgebase)

This repository hosts the knowledge base for FoxTail Android educational platform.  
For more information see TODO WEBSITE LINK

## Contributing

TODO Contributing guide and rules

## Thanks to
Facebook for providing [Docusaurus Platform](https://docusaurus.io/) which this knowledge base runs on.
