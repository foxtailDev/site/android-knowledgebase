# Contributing
When contributing to this repository, we expect you to follow GitLab's [Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/).

---
### Working on requested topic
See issues tab in the GitLab project, look for issues with a tag `To Do` or `To Do priority`.  
If you want to work on a selected issue please set a tag `Doing` and assign the issue to yourself.

### Suggesting changes
If you want to suggest any change please create issue and tag it as `Suggestion`.  
Moderators will move the issue into `To Do` if we deem it as a good topic and anyone can start working on it.  

Minor changes like grammar, typo, or syntax fixes can be made without suggesting a change, just submit a MR and a moderator will assess the change.

## Working with files
When editing or creating files, please make sure to use correct markdown language. A short [guide](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) is available.

In order to create or edit files, it is recommended to use the web IDE.

* To Edit files, navigate the repository and click on any file you wish to edit.
* You can preview the markdown by using the `Preview Markdown` tab in the top of the web editor.
* To Create files, navigate to the target folder and use the "+" option on the taskbar.  
    The window that will open up will contain two main sections, the title and body of the text, and commit message.  
    A good commit message helps us mantain future changes and speed up the review process. You can find a short guideline [here](https://github.com/erlang/otp/wiki/writing-good-commit-messages). 

Any document has to contain a header specifying the title of the document and optionally a sidebar label.  
The title is showed when the document is opened on the top.  
Sidebar label shows on the left navigation menu. If no sidebar label is specified, the title is used.
The correct formatting of the header is the following:
```
---
title: My Document
sidebar_label: Document
---
```

### List of important files
docs directory contains all the documents.
website/static/img contains images.
website/static/img/docs contains images for documents, please choose good image names.
