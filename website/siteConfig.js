// See https://docusaurus.io/docs/site-config for all the possible
// site configuration options.

const users = [
];

const siteConfig = {
  title: 'FoxTail knowledgebase',
  tagline: 'FoxTail Android knowledgebase',
  url: 'https://foxtaildev.gitlab.io/',
  baseUrl: '/site/android-knowledgebase/',

  projectName: 'android-knowledgebase',
  organizationName: 'FoxTail',
  
  // For no header links in the top nav bar -> headerLinks: [],
  headerLinks: [
    {doc: 'getting_started-welcome', label: 'Docs'},
    {page: 'help', label: 'Help'},
    // {blog: true, label: 'Blog'},
  ],

  // If you have users set above, you add it here:
  users,

  /* path to images for header/footer */
  headerIcon: 'img/docusaurus.svg',
  footerIcon: 'img/docusaurus.svg',
  favicon: 'img/favicon.png',

  /* Colors for website */
  colors: {
    primaryColor: '#6a1b9a',
    secondaryColor: '#c4001d',
  },

  /* Custom fonts for website */
  /*
  fonts: {
    myFont: [
      "Times New Roman",
      "Serif"
    ],
    myOtherFont: [
      "-apple-system",
      "system-ui"
    ]
  },
  */

  // This copyright info is used in /core/Footer.js and blog RSS/Atom feeds.
  copyright: `Copyright © ${new Date().getFullYear()} FoxTail`,

  highlight: {
    // Highlight.js theme to use for syntax highlighting in code blocks.
    theme: 'default',
  },

  // Add custom scripts here that would be placed in <script> tags.
  scripts: ['https://buttons.github.io/buttons.js'],

  // On page navigation for the current documentation page.
  onPageNav: 'separate',
  // No .html extensions for paths.
  cleanUrl: true,

  // Open Graph and Twitter card images.
  ogImage: 'img/docusaurus.png',
  twitterImage: 'img/docusaurus.png',

  // Show documentation's last contributor's name.
  // enableUpdateBy: true,

  // Show documentation's last update time.
  // enableUpdateTime: true,

  repoUrl: 'https://foxtaildev.gitlab.io/site/android-knowledgebase/',
  docsSideNavCollapsible: true,
};

module.exports = siteConfig;
